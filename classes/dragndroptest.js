class DragndropTest {
    constructor(window, json, externalDispatcher) {
        this.window = window;
        this.externalDispatcher = externalDispatcher;
        this.zones = []
        this.items = []
        this.prerenderPage()
        /*
        this.preloadData(json).then((res) => {
            console.log("just string!")
            this.renderPage(this.window, res)
        }) */
        this.json = json;
        this.renderPage();
        window.addEventListener("resize", () => {
            this.recalcMaxWidth()
        })


    }

    isJSONString(variable) {
        try {
          const parsedJSON = JSON.parse(variable);
          return typeof parsedJSON === 'object';
        } catch (error) {
          return false;
        }
      }

    prerenderPage() {
        this.body = document.createElement("div")
        this.body.className = "dragndrop__body"
        const testTitle = document.createElement("div")
        testTitle.textContent = "TECT"
        testTitle.className = "test-title"
        this.body.appendChild(testTitle)

        this.textContainer = document.createElement("div")
        this.textContainer.className = "dragndrop__text";

        this.container = document.createElement('div');
        this.container.className = "dragndrop__container";

        this.bottomContainer = document.createElement("div")
        this.bottomContainer.className = "dragndrop__bottom-container";

        this.checkButton = document.createElement("button")
        this.checkButton.className = "dragndrop__button";
        this.checkButton.onclick = () => {
            this.checkTest()
        }

        this.retryButton = document.createElement("button")
        this.retryDescription = document.createElement("div")
        this.retryDescription.className = "dragndrop__retry-description"
        this.retryButton.className = "dragndrop__button";
        this.retryButton.onclick = () => {
            this.resetTest()
        }

        this.bottomContainer.appendChild(this.retryDescription)
        this.bottomContainer.appendChild(this.checkButton)
        this.bottomContainer.appendChild(this.retryButton)


        this.backgroundContainer = document.createElement('div');
        this.backgroundContainer.className = "dragndrop__background-container";

        this.zonesContainer = document.createElement('div');
        this.zonesContainer.className = "dragndrop__zones-container dragndrop__hide_borders";

        this.itemsContainer = document.createElement('div');
        this.itemsContainer.className = "dragndrop__items-container";

        this.background = document.createElement('div');
        this.background.className = "dragndrop__background";

        this.backgroundImage = document.createElement('img');
        this.backgroundImage.className = "dragndrop__background-image";

        this.background.appendChild(this.backgroundImage)
        this.backgroundContainer.appendChild(this.zonesContainer)
        this.backgroundContainer.appendChild(this.background)
        this.container.appendChild(this.backgroundContainer)
        this.container.appendChild(this.itemsContainer)
        this.window.document.body.appendChild(this.body);
        this.body.appendChild(this.textContainer);
        this.body.appendChild(this.container);
        this.body.appendChild(this.bottomContainer);
    }
/*
    async preloadData(json) {
        this.json = await fetch(json).then(res => res.json())
    }
*/

    renderPage() {
        if (!this.json) {
            return
        }
        this.json.zonesOffset = this.json.zonesOffset || ["0%", "0%"]
        this.textContainer.textContent = this.json.title;
        this.correctAnswer = this.json.correctOrder
        this.backgroundImage.src = this.json.background;
        this.backgroundContainer.style.width = this.json.backgroundWidth;
        this.itemsContainer.style.width = this.json.itemsContainerWidth;
        this.zonesContainer.style.paddingLeft = this.json.zonesOffset[0]
        this.zonesContainer.style.paddingTop = this.json.zonesOffset[1]
        this.container.style.maxWidth = this.json.containerMaxWidth || "auto"
        if (this.json.textBottomPadding) {
            this.textContainer.style.paddingBottom = this.json.textBottomPadding || 0
        }
        this.retryButton.textContent = this.json.retryButtonTitle
        this.checkButton.textContent = this.json.checkButtonTitle
        this.retryDescription.textContent = this.json.retryDescription
        this.correctAnswer.forEach((zone, index) => {
            const item = document.createElement("div")
            const icon = document.createElement("div")
            const iconImage = document.createElement("img")
            const iconOffsets = this.json.iconsOffsets && (this.json.iconsOffsets[index] || this.json.iconsOffsets[0]) || [0, 0]
            icon.style.left = iconOffsets[0]
            icon.style.top = iconOffsets[1]
            icon.appendChild(iconImage)
            icon.className = 'dragndrop__zone-result-icon-container'
            item.className = "dragndrop__zone"
            const zoneOffsets = this.json.zonesOffsets[index] || this.json.zonesOffsets[0]
            item.style.left = zoneOffsets[0]
            item.style.top = zoneOffsets[1]
            item.style.width = this.json.zonesWidths[index] || this.json.zonesWidths[0]
            item.style.width = this.json.zonesWidths[index] || this.json.zonesWidths[0]
            item.style.height = this.json.zonesHeights[index] || this.json.zonesHeights[0]
            item.appendChild(icon)
            this.zones.push(item)
            this.zonesContainer.appendChild(item)
        })
        this.json.items.forEach((itemSrc, index) => {
            const item = document.createElement("img")
            item.addEventListener("mouseenter", () => {
                item.style.opacity = 0.5
            })
            item.addEventListener("mouseleave", () => {
                item.style.opacity = 1
            })
            item.src = itemSrc
            item.style.width = this.json.itemsWidths && this.json.itemsWidths[index] || "100%"
            item.draggable = false
            item.className = "dragndrop__item"
            this.items.push(item)
            this.itemsContainer.appendChild(item)
        })

        Promise.all([...this.items.map(it => new Promise(resolve => {
            it.onload = () => {
                resolve(true)
            }
        })), new Promise(resolve => {
            this.backgroundImage.onload = () => {
                resolve(true)
            }
        })]).then(() => {
            console.log("allLoad")
            const containerRect = this.container.getBoundingClientRect()
            this.containerRatio = containerRect.width / containerRect.height
            this.recalcMaxWidth()
            this.dragndropController = new DragndropController(this.window, this.zones, this.items, () => {
                this.hideDropZones()
            }, () => {
                this.showDropZones()
            }, () => {
                console.log("SELECT CHANGE", this.dragndropController.answers)
                if (this.dragndropController.answers.filter(it => it !== undefined).length === this.correctAnswer.length) {
                    console.log("ALL SETTLED")
                    this.showCheckButton()
                } else {
                    this.hideCheckButton()
                }
            }, this.json)
        })


    }

    recalcMaxWidth = () => {
        if (this.containerRatio && this.container) {
            console.log([...this.body.children])
            const availableHeight = this.body.getBoundingClientRect().height - [...this.body.children].filter(it => it !== this.container).reduce((acc, it) => acc + it.getBoundingClientRect().height, 0)
            const availableWidth = availableHeight * this.containerRatio
            this.container.style.maxWidth = availableWidth + "px"
        }
    }

    renderSuccess() {
        const successContainer = document.createElement("div")
        const successTitle = document.createElement("div")
        const successText = document.createElement("div")
        const testTitle = document.createElement("div")
        testTitle.textContent = "TECT"
        testTitle.className = "test-title"
        successContainer.appendChild(testTitle)
        successContainer.className = "dragndrop_success-container"
        successTitle.className = "dragndrop_success-title"
        successText.className = "dragndrop_success-text"

        if (this.json.successBackground) {
            successContainer.style.background = `url(${this.json.successBackground})`
        }
        successContainer.appendChild(successTitle)
        successContainer.appendChild(successText)

        successTitle.textContent = this.json.successTitle
        successText.textContent = this.json.successText
        this.window.document.body.replaceWith(successContainer)
    }

    checkTest = () => {
        console.log("check test!")
        const result = this.correctAnswer.map((it, index) => it === this.dragndropController.answers[index])
        if (result.every(it => it)) {
            this.showSuccess()
        } else {
            result.forEach((isSuccess, index) => {
                this.dragndropController.enabled = false
                this.showZoneIcon(index, isSuccess)
            })
          this.externalDispatcher.emitResult({result: false})

            this.showRetryButton()
        }
    }
    showZoneIcon = (zoneIndex, success) => {
        const icons = this.zones[zoneIndex].getElementsByClassName("dragndrop__zone-result-icon-container")
        const icon = icons.item(0)
        if (icon) {
            const image = icon.getElementsByTagName("img").item(0)
            image.src = success ? this.json.successIcon : this.json.crossIcon
            icon.style.opacity = 1
        }
    }
    hideAllIcons = () => {
        for (let zone of this.zones) {
            const icon = zone.getElementsByClassName("dragndrop__zone-result-icon-container")[0]
            if (icon) {
                icon.style.opacity = 0
            }
        }
    }

    showCheckButton = () => {
        this.checkButton.style.display = "flex"
    }
    hideCheckButton = () => {
        this.checkButton.style.display = "none"
    }
    showRetryButton = () => {
        this.hideCheckButton()
        this.retryDescription.style.display = "block"
        this.retryButton.style.display = "flex"

    }
    hideRetryButton = () => {
        this.retryDescription.style.display = "none"
        this.retryButton.style.display = "none"

    }
    showSuccess = () => {
        this.renderSuccess()
        this.externalDispatcher.emitResult({ result: true })
    }
    resetTest = () => {
        this.dragndropController.resetTest()
        this.hideRetryButton()
        this.hideCheckButton()
        this.hideAllIcons()

    }
    showDropZones = () => {
        if (this.json.showZones) {
            this.zonesContainer.classList.remove("dragndrop__hide_borders")
        }
    }
    hideDropZones = () => {
        if (this.json.showZones) {
            this.zonesContainer.classList.add("dragndrop__hide_borders")
        }

    }


}

class DragndropController {

    constructor(window, zones, items, onDrop, onDrag, onSelectChange, config) {
        window.document.addEventListener("dragover", function (event) {

            // prevent default to allow drop
            event.preventDefault();

        }, false);
        this.window = window
        this.json = config
        this.answers = []
        this.enabled = true
        this.dragged = null
        this.items = items
        this.onSelectChange = () => {
            onSelectChange(this.answers)
        }
        this.onDrag = onDrag
        this.onDrop = onDrop

        this.window.document.onloadeddata = () => {
            console.log("load")
        }
        console.log(this.window)
        this.draggedDefaultPositions = this.items.map(it => it.getBoundingClientRect())

        this.zones = zones
        this.items.forEach((item, index) => {
            item.addEventListener("mousedown", ev => {
                this.onTouchStart(item, index, ev)
            })
            item.addEventListener("touchstart", ev => {
                const touches = ev.targetTouches[0]
                this.onTouchStart(item, index, touches)
            })
            item.addEventListener("touchend", ev => {
                const touches = ev.targetTouches[0]

                this.onTouchEnd(touches)

            })

            this.window.addEventListener("touchmove", ev => {
                const touches = ev.targetTouches[0]

                this.onTouchMove(touches)
            })
        })
        this.window.addEventListener("resize", () => {
            this.recalcItemsPositions()
        })


        this.window.addEventListener("mouseup", ev => {
            this.onTouchEnd(ev)
        })

        this.window.addEventListener("mousemove", ev => {
            this.onTouchMove(ev)
        })
    }

    onTouchStart = (item, index, ev) => {
        console.log(ev)

        if (this.enabled) {

            this.onDrag()
            item.style.opacity = 0.5
            item.style.transition = "none"

            this.dragged = {item, index}
            const rect = item.getBoundingClientRect()

            const offsetX = ev.pageX - rect.left,
                offsetY = ev.pageY - rect.top;
            this.draggedMousePosition = {x: offsetX, y: offsetY}

            console.log(this.draggedMousePosition, ev)
        }

    }
    onTouchMove = (ev) => {
        if (this.enabled) {
            if (this.dragged) {
                this.translateDraggedItem({
                    x: ev.clientX - this.draggedMousePosition.x, y: ev.clientY - this.draggedMousePosition.y
                })

            }
        }
    }

    onTouchEnd = (ev) => {

        if (this.enabled) {
            this.onDrop()
            if (this.dragged) {
                const inZone = this.isInZone(ev)
                const {index} = this.dragged
                this.dragged.item.style.opacity = 1

                const prevIndex = this.answers.findIndex(it => it === index)
                if (prevIndex !== -1) {
                    this.answers[prevIndex] = undefined
                }
                if (inZone && inZone.index !== undefined && !Number.isInteger(this.answers[inZone.index]) &&
                    ((!this.json.onlyCorrectPlaces) || this.json.correctOrder[inZone.index] === index)) {

                    this.translateDraggedItem(inZone.zone.getBoundingClientRect())

                    this.answers[inZone.index] = index
                    this.onSelectChange()
                } else {
                    this.dragged.item.style.transition = `transform 0.5s`
                    this.resetDraggedItem()
                }
            }
            this.dragged = null
        }
    }

    recalcItemsPositions() {
        this.answers.forEach(it => {
            if (it !== undefined) {
                this.items[it].style.transition = `none`
                this.items[it].style.transform = `translate(0,0)`

            }
        })
        this.draggedDefaultPositions = this.items.map(it => it.getBoundingClientRect())

        this.answers.forEach((it, index) => {

            if (it !== undefined) {
                const itemRect = this.draggedDefaultPositions[it]
                const zoneRect = this.zones[index].getBoundingClientRect()
                this.items[it].style.transform = `translate(
                ${-itemRect.x + zoneRect.x}px,
                ${-itemRect.y + zoneRect.y}px)`
            }
        })
    }

    resetTest() {
        this.enabled = true
        for (let i = 0; i < this.answers.length; i++) {
            this.answers[i] = undefined
        }
        this.dragged = null
        for (let item of this.items) {
            item.style.transition = `transform 0.5s`
            item.style.transform = "translate(0,0)"
        }
    }

    translateDraggedItem(to) {
        if (this.dragged) {
            const {index} = this.dragged
            this.dragged.item.style.transform =
                `translate(${-this.draggedDefaultPositions[index].left + to.x}px,
                    ${-this.draggedDefaultPositions[index].top + to.y}px)`
        }
    }

    resetDraggedItem() {
        const selectedIndex = this.answers.find(it => it === this.dragged.index)
        if (selectedIndex !== -1) {
            this.answers[selectedIndex] = undefined
            this.onSelectChange()
        }
        this.dragged.item.style.transform = "translate(0,0)"

    }

    isInZone(ev) {
        const draggedRect = this.dragged && this.dragged.item.getBoundingClientRect()
        let i = 0
        for (let zone of this.zones) {
            if (isEventInZone({
                pageX: draggedRect.x + (draggedRect.width / 2),
                pageY: draggedRect.y + (draggedRect.height / 2)
            }, zone)) {
                return {zone, index: i}
            }
            i++
        }
        return undefined
    }

}

const isEventInZone = (ev, zone) => {
    const {x, y, width, height} = zone.getBoundingClientRect()
    return ev.pageX > x && ev.pageY > y && ev.pageX < x + width && ev.pageY < y + height;
}
