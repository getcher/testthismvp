class SomeTest {
    constructor(window, json, externalDispatcher) {
    this.window = window;
    this.json = json;
    this.externalDispatcher = externalDispatcher;
    this.element = document.createElement('div');
    this.element.textContent = 'hello world';
    this.window.document.body.appendChild(this.element);
    this.simulateResult();
    }
    
    simulateResult() {
        // Simulate test completion after a delay
       setTimeout(() => {
         this.externalDispatcher.emitResult({ result: 'Test completed!' });
       }, 2000);
     }

  }
