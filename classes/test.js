class Test {
    constructor(options, container) {
        this.options = options;
        this.container = container;

        this.externalDispatcher = new ExternalDispatcher();

        this.initialize();
    }

    initialize() {
        console.log("Test initialization");

        const iframe = document.createElement('iframe');
        iframe.width = '100%';
        iframe.height = '100%';
        this.container.appendChild(iframe);
        const link = document.createElement('link');
        link.rel = 'stylesheet';
        link.type = 'text/css';
        link.href = this.options.style; // Specify the path to your custom CSS file
        iframe.contentDocument.head.appendChild(link);

        iframe.addEventListener("load",()=>{
            iframe.contentDocument.head.appendChild(link);

            switch (this.options.type) {//FIREFOX FIX
                case "imagetest": {
                    const imagesTest = new ImagesTest(iframe.contentWindow.window, this.options.json, this.externalDispatcher);
                    break
                }
                case "dragndroptest": {
                    const dragndropTest = new DragndropTest(iframe.contentWindow.window, this.options.json, this.externalDispatcher);
                    break
                }
            }
        })
        // Check the type value in options and add ImagesTest object to container if it matches
        switch (this.options.type) {
            case "imagetest": {
                const imagesTest = new ImagesTest(iframe.contentWindow.window, this.options.json, this.externalDispatcher);
                break
            }
            case "dragndroptest": {
                const dragndropTest = new DragndropTest(iframe.contentWindow.window, this.options.json, this.externalDispatcher);
                break
            }
        }

    }
}

class ExternalDispatcher {
    constructor() {
        this.listeners = {};
    }

    on(event, callback) {
        // Register a callback for the specified event
        if (!this.listeners[event]) {
            this.listeners[event] = [];
        }
        this.listeners[event].push(callback);
    }

    emit(event, payload) {
        // Trigger all the callbacks registered for the specified event
        const eventListeners = this.listeners[event];
        if (eventListeners) {
            eventListeners.forEach(callback => callback(payload));
        }
    }

    emitResult(result) {
        // Emit the "result" event with the provided result payload
        this.emit('result', result);
    }
}

