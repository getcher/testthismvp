class ImagesTest {
	constructor(window, json, externalDispatcher) {
        this.window = window;
        this.externalDispatcher = externalDispatcher;
        this.config = json;
        this.renderPage();
    }
	
	renderPage() {
		var externalDispatcher = this.externalDispatcher;
        this.body = document.createElement("div")
        this.body.className = "imagetest__body"
        this.testTitle = document.createElement("div")
        this.testTitle.className = "test-title"
		
		this.winWrapper = document.createElement("div")
		this.winWrapper.className = "win__wrapper";
		
		this.winText = document.createElement("div")
		this.winText.className = "win_text";
		this.winText.innerHTML = this.config.wintext;
		
		
		this.body.appendChild(this.testTitle)
		this.body.appendChild(this.winWrapper)

        this.questWrapper = document.createElement("div")
        this.questWrapper.className = "quest__wrapper";
		this.body.appendChild(this.questWrapper)

        this.question = document.createElement('div');
        this.question.className = "question";

        this.answersWrapper = document.createElement("div")
        this.answersWrapper.className = "answers__wrapper";
		
		this.mistake = document.createElement("div")
		this.mistake.className = "mistake";
		this.mistake.textContent = "Не совсем верно."

		if (!this.config) {
            return
        }
		var allowSelect = true;
		
		this.testTitle.textContent = this.config.title;
		this.question.textContent = this.config.question;

        this.checkButton = document.createElement("button")
        this.checkButton.className = "check__button";
        this.checkButton.classList.add("disabled");
        this.checkButton.textContent = "Проверить";
		var anwers = this.config.answers;
        this.checkButton.addEventListener("click", function(){
			var result;
			if (this.classList.contains("disabled")) return;
			else if (this.classList.contains("retry")){
				window.document.querySelector("IFRAME").contentWindow.document.querySelector(".check__button").classList.add("disabled");
				window.document.querySelector("IFRAME").contentWindow.document.querySelector(".imagetest__body").classList.remove("incorrect");
				window.document.querySelector("IFRAME").contentWindow.document.querySelector(".check__button").classList.remove("retry");
				if (document.querySelector(".answer.active")) document.querySelector(".answer.active").classList.remove("active");
				window.document.querySelector("IFRAME").contentWindow.document.querySelector(".check__button").textContent = "Проверить";
				window.document.querySelector("IFRAME").contentWindow.document.querySelector(".answer.active").classList.remove("active");
				allowSelect = true;
			} else {
				allowSelect = false;
				var win = false;
				var answer = window.document.querySelector("IFRAME").contentWindow.document.querySelector(".answer.active").getAttribute("data-answer");
				for (var a=0; a<anwers.length; a++){
					if (anwers[a].correct && a==answer) win = true;
				}
				result = answer;
				if (win){
					window.document.querySelector("IFRAME").contentWindow.document.querySelector(".imagetest__body").classList.remove("incorrect");
					window.document.querySelector("IFRAME").contentWindow.document.querySelector(".imagetest__body").classList.add("correct");
					externalDispatcher.emitResult({result: true})
				} else {
					window.document.querySelector("IFRAME").contentWindow.document.querySelector(".imagetest__body").classList.add("incorrect");
					window.document.querySelector("IFRAME").contentWindow.document.querySelector(".check__button").textContent = "Попробуй еще раз";
					window.document.querySelector("IFRAME").contentWindow.document.querySelector(".check__button").classList.add("retry");
					externalDispatcher.emitResult({result: false})
				}
			}
		});
		
		this.winWrapper.appendChild(this.winText)
		this.questWrapper.appendChild(this.question)
		this.questWrapper.appendChild(this.answersWrapper)
		this.questWrapper.appendChild(this.mistake)
		this.questWrapper.appendChild(this.checkButton)

        this.body.appendChild(this.checkButton)
		
		
		for (var a=0; a<this.config.answers.length; a++){
			this.answer = document.createElement("DIV");
			this.answer.classList.add("answer");
			this.answer.setAttribute("data-answer", a);
			this.answerImage = document.createElement("IMG");
			this.answerImage.classList.add("answer-pict");
			this.answerImage.setAttribute("src", this.config.answers[a].pictURL);
			this.answer.appendChild(this.answerImage);
			this.answersWrapper.appendChild(this.answer);
			this.answer.addEventListener("click", function(){
				if (!allowSelect) return;
				if (window.document.querySelector("IFRAME").contentWindow.document.querySelector(".answer.active")) window.document.querySelector("IFRAME").contentWindow.document.querySelector(".answer.active").classList.remove("active");
				this.classList.add("active");
				window.document.querySelector("IFRAME").contentWindow.document.querySelector(".check__button").classList.remove("disabled");
			});
			
			
		}
		
		
		this.window.document.body.appendChild(this.body);
    }
	
	async preloadData(json) {
        this.config = await fetch(json).then(res => res.json())
    }
}